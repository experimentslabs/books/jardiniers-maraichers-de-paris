% !TeX root = ./00_00_main_book.tex
\chapter{
	Histoire sommaire de la culture maraîchère à Paris,
	depuis cinquante ou soixante ans
}

	Il serait désirable, sans doute, de nous voir commencer cet ouvrage
par des recherches sur l'origine de la culture maraîchère à Paris,
de nous voir suivre et expliquer ses progrès successifs depuis son
commencement jusqu'à nos jours; mais une telle recherche n'est pas
en notre pouvoir; nous n'avons ni le temps ni le moyen de nous élever
au delà de mémoire d'homme, et nous ne pouvons ni ne voulons dire
ici autre chose que ce que nous avons vu et faisons nous-mêmes.

	Il est généralement connu que, toutes les fois
qu'on a reculé l'enceinte de Paris, les jardiniers-maraîchers ont
été obligés de se reculer aussi pour faire place à de nouvelles bâtisses,
et que ce déplacement leur était toujours onéreux, en ce qu'ils quittaient
un terrain amélioré de longue main pour aller s'établir sur un nouveau
sol, souvent rebelle à leur culture, et qui ne pouvait être amélioré
qu'avec le temps et de grandes dépenses.
Plusieurs d'entre nous se rappellent que, vers la fin du siècle dernier,
lorsqu'on a reporté le mur d'enceinte où sont aujourd'hui les barrières
de l'octroi, beaucoup de maraîchers sont allés s'établir au delà des
nouvelles barrières, parce que le prix des terrains circonscrits a
de suite considérablement augmenté. Plus tard, lors de l'établissement
du canal Saint-Martin, un assez grand nombre de maraîchers furent
encore obligés d'aller s'établir plus loin pour laisser la place aux
nouvelles constructions. Jusqu'en 1780, on voyait des jardins maraîchers
le long du boulevard, depuis la porte Saint-Antoine, aujourd'hui place
de la colonne de Juillet, jusque près de la Madeleine; depuis longtemps
on n'y en voit plus aucun. Le trentième quartier de Paris s'appelait
alors le \emph{Pont-aux-Choux}, tant les marais y étaient nombreux. Enfin,
jusqu'à la révolution de 89, les jardiniers-maraîchers ont conservé
l'usage d'appeler leur jardin marais; mais depuis lors, la plupart
disent leur jardin, parce qu'en effet ces jardins ne ressemblent
plus aux marais dans lesquels les premiers maraîchers s'étaient établis,
d'où le nom de maraîchers que leurs successeurs portent toujours.

	Il conviendrait à présent de jeter les yeux en arrière pour savoir
quels étaient nos aïeux et en quel état était la profession de jardinier-maraîcher
entre leurs mains; mais les maraîchers ne connaissent d'autre chronique
que la tradition qui se transmet oralement parmi eux, et leurs souvenirs
ne remontent guère au delà de deux ou trois générations. Nous ne pouvons
donc remonter nous-mêmes au delà d'une soixantaine d'années,
dans le temps qui a précédé celui dans lequel nous vivons, à moins
de nous rendre l'écho de ce qu'ont écrit les autres, chose que nous
éviterons toujours.

	D'après le témoignage de nos plus anciens confrères, il résulte que,
il y a 60 ou 80 ans, la culture maraîchère était beaucoup moins perfectionnée
qu'aujourd'hui; que l'on faisait moins de saisons dans une année;
que l'art des primeurs était encore dans l'enfance; que les plus
habiles maraîchers n'avaient encore que des cloches, et en petit nombre,
pour avancer leurs légumes et surtout pour élever une sorte de melon
brodé, la seule qu'ils connussent alors, et qui aujourd'hui porte
encore le nom de melon maraîcher. Ce n'est pas qu'à cette époque l'art
des primeurs et l'emploi des châssis fussent ignorés à Paris; depuis
longtemps l'un et l'autre étaient en progrès dans les jardins royaux
et chez plusieurs grands seigneurs; mais
ils n'avaient pas encore pénétré dans la culture maraîchère, lorsqu'en
1780 un jardinier-maraîcher nommé Fournier fit le premier usage de
châssis dans sa culture avec un succès si prononcé pour obtenir des
primeurs beaucoup plus tôt qu'auparavant, qu'un grand nombre de ses
confrères l'imitèrent et en obtinrent de suite de grands avantages.
L'usage et la manœuvre des châssis devinrent une branche importante
de la culture maraîchère, et c'est de cette époque que le terme
\emph{culture forcée} est devenu familier parmi nous.

	Le même Fournier qui a introduit les châssis dans la culture maraîchère,
en 1780, y a aussi introduit, quelques années après, la culture
du melon cantaloup; il est aussi le premier maraîcher qui ait cultivé
la patate.

	Le premier qui a forcé l'asperge blanche était un nommé Quentin, vers
1792.

	L'asperge verte a commencé à être forcée par le même et par son beau-frère,
nommé Marie, vers 1800.

	Celui qui le premier a forcé le chou-fleur est le nommé Besnard, vers
1811.

	Les premières romaines forcées l'ont été par MM. Dulac et Chemin,
vers 1812.

	La chicorée fine d'Italie a commencé à être forcée, à la même époque,
par Baptiste Quentin.

	Parmi les jardiniers-maraîchers de notre époque, ce sont les frères
Quentin, Fanfan et Baptiste,
ainsi que M. Dulac, qui les premiers ont traité le haricot en culture
forcée, en 1814.

	La culture forcée de la carotte a eu lieu, pour la première fois, en
1826; elle est due à M. Pierre Gros.

	En 1836, M. Gontier a, le premier, fait usage du thermosiphon dans la
culture forcée sous châssis.

	Nous venons de nommer cinq maraîchers de notre époque, parce qu'ils
ont enrichi la culture maraîchère par des procédés nouveaux, et que
la reconnaissance demande que leurs noms ne soient pas plus oubliés
que ceux que nous avons nommés auparavant. Nous voudrions bien nommer
aussi ceux de nos confrères qui se distinguent par la perfection de
leur culture, par l'étendue de leur exploitation, par leur activité,
leur adresse, leur bonne administration; mais le nombre en
serait trop grand: d'ailleurs, en faisant ressortir le mérite d'une
partie de nos confrères et en passant l'autre sous silence, cela paraîtrait,
de notre part, une partialité dont nous ne voulons pas qu'on nous accuse,
mais nous dirons avec plaisir, avec orgueil même, qu'il y a dans
la classe maraîchère plus de capacité, plus d'intelligence qu'on ne
le croit généralement dans les autres classes.

	Voilà tout ce que nous pouvons dire de la culture maraîchère des temps
qui nous ont précédés, et de ses progrès jusqu'à ce jour. Ce sont des
faits certains, dont plusieurs se sont accomplis sous nos yeux et
desquels nous profitons aujourd'hui.
