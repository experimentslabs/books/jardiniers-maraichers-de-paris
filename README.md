# Manuel pratique de la culture maraîchère de Paris (ed. 1845)

**LICENSE**: Cet ouvrage est dans le domaine public. Les documents
de ce dépôt proviennent de la Bibliothèque Nationale de France.

Il est donc soumis aux [conditions d'utilisation de la BNF](https://gallica.bnf.fr/edit/conditions-dutilisation-des-contenus-de-gallica).

Source du document: https://gallica.bnf.fr/ark:/12148/bpt6k9774172h

---

Le présent dépôt contient la version relue et corrigée, remise en forme
pour être utilisée dans un but non commercial.
