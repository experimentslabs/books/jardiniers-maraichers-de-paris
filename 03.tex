% !TeX root = ./00_00_main_book.tex
\chapter{Des habitudes et manière d'être des maraîchers de Paris}
	Parler des jardiniers-maraîchers, c'est parler de nous-mêmes; or
il est assez difficile de parler de soi quand on a du bien et beaucoup
de bien à en dire: cependant, la nature de notre ouvrage nous obligeant
à dire les habitudes et les mœurs des maraîchers, nous allons les
dire franchement, en nous effaçant personnellement autant qu'il nous sera possible.

	Les maraîchers de Paris forment la classe de travailleurs la plus
laborieuse, la plus constante, la plus paisible de toutes celles
qui existent dans la capitale. Quelque dur, quelque pénible, que soit
son état, on ne voit jamais le maraîcher le quitter pour en prendre
un autre. Les fils d'un maraîcher s'accoutument au travail, sous les
yeux et à l'exemple de leur père, et presque tous s'établissent maraîchers.
Les filles se marient rarement à un homme d'une autre profession que
celle de leur père.

	Quoique le métier soit très-dur, le maraîcher s'y attache; quelque
multipliées que soient ses fatigues et ses veilles, elles ne lui paraissent
jamais trop pénibles; quand même l'inclémence des saisons vient contrarier
ses projets, il se flatte d'être plus heureux une autre fois; il ne
désespère jamais de la Providence.

	Nous sommes persuadés même que c'est à la confiance qu'ils ont en
la Providence que les maraîchers de Paris doivent la tranquillité,
le bon accord qui existent parmi eux. Les ressorts qui font remuer
les passions chez les autres hommes leur sont inconnus; leur seule
ambition, à eux, est de chercher les moyens d'arriver les premiers
à porter des primeurs à la halle: une telle ambition ne troublera
certainement jamais la sûreté publique...

	Il s'en faut de beaucoup que la classe maraîchère reste routinière
et stationnaire, comme on le croit
généralement. Les maraîchers suivent les progrès, les perfectionnements
du siècle; leur bien-être, leur aisance s'augmentent en raison de
l'étendue de leur intelligence et de la justesse de leur raisonnement.

	Il y a à peine quarante ans, les maraîchers étaient mal logés, mal
vêtus; ils se nourrissaient mal, ils portaient presque tous, sur
leur dos, les légumes à la halle; ils tiraient l'eau de leur puits
à la corde et à force de bras. Aujourd'hui les maraîchers sont mieux
vêtus, ils se nourrissent mieux, ils ont, presque tous, un cheval
et une voiture pour mener les légumes à la halle et amener les fumiers;
au lieu de tirer l'eau à force de bras, les maraîchers ont généralement
un manège ou une pompe qui fournit de l'eau en abondance.

	Mais, si le maraîcher a amélioré son existence, s'il se nourrit mieux,
s'il est mieux vêtu qu'autrefois, si même il est devenu propriétaire
de son marais, c'est qu'il travaille plus, qu'il travaille mieux,
et surtout avec plus d'intelligence qu'autrefois. Le maraîcher, en
effet, pendant sept mois de l'année, travaille dix-huit ou vingt heures
sur vingt-quatre, et, pendant les cinq autres mois, ceux d'hiver,
il travaille quatorze et seize heures par jour, et, bien souvent encore,
il se lève la nuit pour interroger son thermomètre, pour doubler les
couvertures des cloches et des châssis qui renferment ses plus chères
espérances, son avenir, qu'un degré de gelée peut anéantir.

	Depuis vingt et trente ans l'intelligence des maraîchers s'est particulièrement
portée vers les moyens de forcer la nature à produire, au milieu de
l'hiver, au milieu des frimas, ce que, dans sa marche ordinaire, elle
ne produit que dans les beaux jours du printemps ou de l'été, et c'est
en cela que la science des maraîchers de Paris est devenue véritablement
étonnante. Dès le mois de novembre, et souvent dès octobre, ils fournissent
à la consommation des asperges blanches et presque toute l'année des
asperges vertes; en janvier, des laitues pommées en abondance; en
février, des romaines; en mars, des carottes nouvelles, des raves,
des radis et du cerfeuil nouveau, des fraises, etc.;
en avril, des tomates, des haricots, des melons, etc.

	Avant l introduction des cultures forcées dans les marais de Paris,
la classe maraîchère, toujours respectable d'ailleurs par son utilité
et la pureté de ses mœurs, ne jouissait que d'une faible considération:
un maraîcher alors n'était guère recherché en dehors de sa classe;
aujourd'hui il n'en est plus ainsi; le maraîcher qui a la réputation
d'être habile dans la culture des primeurs voit souvent un équipage
à sa porte et des personnes, considérables par leur rang et leur fortune,
en descendre pour causer avec lui, considérer son travail, étudier
auprès de lui la pratique, et lui demander des avis ou des renseignements
pour les transmettre à leur jardinier.

	Nous nous abstenons ici de développer ce que
la classe maraîchère doit gagner à ces communications, nous nous bornons à
désirer qu'elles deviennent de plus en plus fréquentes.

	Un établissement maraîcher, comme beaucoup d'autres, ne peut guère
prospérer sans femme: si l'homme cultive le marais et le fait produire,
la femme seule sait tirer parti de ses productions; aussi un jeune
maraîcher qui cherche à s'établir commence-t-il par se marier. L'un
reçoit le titre de maître, l'autre celui de maîtresse. S'ils ne reçoivent
pas en dot un marais tout monté, les commencements sont durs pour
l'un et pour l'autre; car, quelle que soit l'exiguïté d'un marais,
les premières dépenses sont considérables: il faut qu'ils prennent
des gens à gages; il faut les nourrir et les coucher, ce qui n'exempte
pas le maître et la maîtresse d'être les premiers et les derniers
à l'ouvrage; il faut qu'ils se montent en coffres, châssis, cloches;
il faut enfin faire un amas considérable de fumier, et ce n'est
que quand ils ont tout cela à discrétion et sous la main que nos jeunes
maraîchers peuvent travailler avec l'espoir de quelque profit. Mais
l'amour du travail est tellement inhérent à la classe maraîchère et
le travail lui-même, quoique violent et prolongé, est apparemment
si salutaire, qu'on voit rarement un jeune établissement ne pas prospérer.

	Le maître maraîcher est toujours à la tête de ses garçons et la maîtresse
à la tête de ses femmes de journée; tandis que les hommes labourent,
plantent, arrosent, font des couches, placent des cloches, des châssis,
les femmes sont dans une activité continuelle; elles ésherbent, elles
cueillent l'oseille, le cerfeuil, les épinards, les mâches, elles
arrachent les laitues, elles lient les romaines, etc.

	Si leur part dans un marais semble moins pénible que celle des hommes,
elle est peut-être bien moins saine; car les femmes sont, par leurs
travaux, une partie de la journée, à genoux ou à moitié couchées sur
la terre, souvent humide, et il en résulte fréquemment pour elles des
fraîcheurs plus ou moins douloureuses.

	Dans les soirées, tandis que les hommes travaillent dehors encore
bien avant dans la nuit, les femmes préparent et montent les voies,
les hottes, les mannes et les mannettes pour la halle du lendemain.
C'est ici, c'est dans la préparation et l'arrangement des légumes,
que le goût et l'adresse de la maîtresse se montrent supérieurs au
goût et à l'adresse du maître.

	Le lendemain, à deux heures du matin en été, à quatre heures en hiver,
tout le monde est debout: la maîtresse part pour la halle avec sa
voiture de marchandises, aidée de la fille ou d'un garçon. Si c'est
dans le temps où certains légumes sont abondants, comme les choux-fleurs,
les melons ou autres, dans la même nuit on lui en renvoie une ou deux
autres voitures.

	C'est à la femme que sont confiés les intérêts de la vente; par la
même raison, tout l'argent des
ventes, pendant toute l'année, passe nécessairement par ses mains.
Il faut donc, pour que l'établissement prospère, que le maraîcher
ait une entière confiance en sa femme et que celle-ci n'en abuse jamais.
La simplicité, la pureté des mœurs de la classe maraîchère, le désir
constant de faire honorablement ses affaires, sont un garant suffisant
contre tout ce qui pourrait troubler l'harmonie du ménage.

	Les maraîchers font tous donner l'éducation primaire et les principes
de la religion à leurs enfants: dès qu'ils peuvent manier la bêche,
les enfants alternent l'étude avec le travail; à l'âge de douze ans,
le père, pour les encourager, leur abandonne un coin de terre où ils
cultivent pour eux ce qui leur paraît le plus profitable. Là ils font
usage de leur jeune expérience, ils s'aident de ce qu'ils ont vu faire
et de ce qu'ils ont fait eux-mêmes pour le compte de leur père, et
comme, pendant que leur plantation croît et grandit, ils entendent
toujours parler d'économie par leur père et leur mère, ils s'accoutument
à ne pas dépenser inutilement le produit de la vente de leur petite
culture, et c'est ainsi qu'aujourd'hui beaucoup d'enfants de maraîchers,
de l'âge de treize à quinze ans, ont déjà des économies placées à
la caisse d'épargne.

	Dans un établissement maraîcher, tout le monde se levant avant le
jour, on mange à sept heures du matin, en travaillant; on déjeune
à dix heures; on dîne à deux heures; on soupe de huit à dix
heures du soir, selon les saisons. Le maître et la maîtresse, les
enfants, la fille et les garçons à gages mangent ensemble à la même
table. Le respect et la décence y sont rigoureusement observés; jamais
on ne profère aucun propos équivoque ou inconvenant devant les enfants;
aussi les garçons contractent-ils l'habitude d'être réservés dans
leurs paroles et s'abstiennent-ils des excès que l'on blâme avec raison
chez les ouvriers des autres classes. Le maître ne prend jamais un
ton de hauteur sur ses garçons; il se rappelle qu'il a été garçon
lui-même. Son autorité ne se fait remarquer que dans la direction
des travaux et pour que chaque chose soit faite à propos.

	Nous ne connaissons pas de rivalité jalouse; nous ne connaissons
qu'une vive, une louable émulation; nous nous portons réciproquement
un véritable intérêt, une amitié franche.

	Trop nombreux pour nous réunir tous ensemble, chaque année, pour
fêter notre patron \emph{saint Fiacre}, nous nous divisons en plusieurs confréries.
Au moyen d'une cotisation, chaque confrérie fait orner et décorer
son église. Le parfum des fleurs s'y mêle à celui de l'encens. On
chante une messe en musique; le prêtre appelle les bénédictions
du ciel sur les travaux des jardiniers.

	Après la cérémonie de l'église, chaque confrérie se réunit à un banquet,
souvent suivi d'un bal, mais qui cesse aussitôt l'heure du départ
pour la halle.

	Une gaieté franche préside toujours à ces fêtes; on n'y voit jamais
aucun désordre, aucun excès: les jardiniers nomment entre eux des
commissaires pour veiller au bon ordre, et rarement ces commissaires
ont besoin de faire usage de leurs pouvoirs.

	Jamais on ne voit ni vieux maraîchers ni vieilles maraîchères avoir
recours à la charité publique, comme on en voit tant d'exemples dans
beaucoup d'autres classes. Ce n'est pas, cependant, que tous les maraîchers
et maraîchères puissent se mettre à l'abri des besoins sur leurs vieux
jours; mais ils sont tellement accoutumés à travailler, qu'ils ne conçoivent
pas qu'on puisse vivre autrement que par le travail: ainsi, ceux qui
n'ont pu faire d'économies, qui n'ont pas de famille ou qui ont éprouvé
des malheurs, et qui n'en éprouve dans la vie? vont, pour un faible
salaire, offrir leurs services à leurs confrères plus heureux, et ceux-ci
vont toujours au-devant d'eux, et toujours se font un devoir de les
accueillir et de les occuper selon leurs forces.
