#!/bin/sh

set -x

# Cleanup
rm *.aux
latexmk 00_00_main_book.tex -C

# Build everything!
latexmk 00_00_main_book.tex -pdf -silent